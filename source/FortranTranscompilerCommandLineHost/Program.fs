﻿// Learn more about F# at http://fsharp.net
// See the 'F# Tutorial' project for more help.
open FortranAst
open FortranLexer
open FortranParser
open System

[<EntryPoint>]
let main argv = 
    if argv.Length = 0 then Console.Out.WriteLine("Usage: f2 Filename") else
        let lexbuf =  Lexing.LexBuffer<byte>.FromString(IO.File.ReadAllText(argv.GetValue(0) :?> String))
        try      
           let Ast =  executable_program tokenize (lexbuf)  
           let Result = Fortran2CSharpTranscompiler.Translate Ast
           Console.Out.WriteLine(Result)
        with e ->        
                    //ParseError(e, lexbuf)  
                    reraise()               
        ()
    0 // return an integer exit code
