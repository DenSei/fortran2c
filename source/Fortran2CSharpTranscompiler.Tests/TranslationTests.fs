﻿module TranslationTests

open NUnit.Framework
open FortranAst
open FortranLexer
open FortranParser
open FsUnit

[<Test()>]
let ``When Empty Function Then Translator generates empty function body``() =
    let Ast = {
                Statements= [
                    Function([Specification([FunctionDeclaration("FunctionA", String, [])])])
                    ]
                }
    let Result = Fortran2CSharpTranscompiler.Translate Ast
    Assert.That(Result, Is.EqualTo("public string FunctionA() {\r\n}\r\n"))


[<Test()>]
let ``When Function has parameters Then Translator generates function with parameters``() =
    let Ast = {
                Statements= [
                    Function([Specification([FunctionDeclaration("FunctionA", String, ["a";"b"])]);
                    Specification([VariableDeclaration(Real, Identifier("a"), [])]);
                    Specification([VariableDeclaration(Int, Identifier("b"), [])])])
                    ]
                }
    let Result = Fortran2CSharpTranscompiler.Translate Ast
    Assert.That(Result, Is.EqualTo("public string FunctionA(double a,int b) {\r\n}\r\n"))

[<Test>]
let ``When simple variable declaration then translator is able to transform``() = 
    let Ast = {
                Statements = [
                    Specification([VariableDeclaration(Real, Identifier("points"), [Restriction(IntValue(4L), IntValue(5L))])]);
                ]
                }
    let Result = Fortran2CSharpTranscompiler.Translate Ast
    Assert.That(Result, Is.EqualTo("double points[];\r\n"))

[<Test>]
let ``When if clause with several conditions then if clause can be transformed``() = 
    let Ast = {
                Statements = [
                    Block([
                        If(Equal(Identifier("a"), Constant(FloatValue(2.9))), [
                            Expression(Assignment(Identifier("b"), Constant(StringValue("Cat"))))
                        ]);
                        If(ElseIf(Greater(Identifier("a"), Constant(FloatValue(2.9)))), [
                            Expression(Assignment(Identifier("b"), Constant(StringValue("Dog"))))
                        ]);
                        If(EmptyExpr, [
                            Expression(Assignment(Identifier("b"), Constant(StringValue("Duck"))))
                        ])
                    ])
                ]
            }
    let Result = Fortran2CSharpTranscompiler.Translate Ast
    Assert.That(Result, Is.EqualTo(@"if(a == 2.9) {
b = ""Cat"";
}
else if(a > 2.9) {
b = ""Dog"";
}
else {
b = ""Duck"";
}
"))

[<Test>]
let ``When simple array instantiation then translator recognizes instantation correctly``() = 
    let Ast = {
                Statements = [
                    Expression(Assignment(Identifier("a"), ObjectInitialization(ArrayType(DoubleType), Constant(IntValue(4L)))))
                ]
            }
    let Result = Fortran2CSharpTranscompiler.Translate Ast
    Assert.That(Result, Is.EqualTo(@"a = new double[4];"))

[<Test>]
let ``When sum function then translator creates linq expression``() = 
    let Result = Fortran2CSharpTranscompiler.BuildArrayAggregateFunction("Sum",[Identifier("a");Greater(Identifier("a"), Constant(IntValue(4L)))], Fortran2CSharpTranscompiler.TranslateExpression) 
    Assert.That(Result, Is.EqualTo(@"a.Where(_a => _a > 4).Sum()"))

[<Test>]
let ``When count function then translator appends length property``() = 
    let Result = Fortran2CSharpTranscompiler.BuildArrayAggregateFunction("Count",[Identifier("a")], Fortran2CSharpTranscompiler.TranslateExpression)  
    Assert.That(Result, Is.EqualTo(@"a.Count()"))

[<TestCase("program average
 
! Read in some numbers and take the average
! As written, if there are no data points, an average of zero is returned
! While this may not be desired behavior, it keeps this example simple
 
  implicit none
  integer :: number_of_points
  real, dimension(:), allocatable :: points
  real :: average_points=0., positive_average=0., negative_average=0.
 
  write (*,*) \"Input number of points to average:\"
  read (*,*) number_of_points
 
  allocate (points(number_of_points))
 
  write (*,*) \"Enter the points to average:\"
  read (*,*) points
 
! Take the average by summing points and dividing by number_of_points
  if (number_of_points > 0) average_points = sum(points)/number_of_points
 
! Now form average over positive and negative points only
  if (count(points > 0.) > 0) positive_average = sum(points, points > 0.) &
        /count(points > 0.)
  if (count(points < 0.) > 0) negative_average = sum(points, points < 0.) &
        /count(points < 0.)
 
  deallocate (points)
 
! Print result to terminal
  write (*,'(''Average = '', 1g12.4)') average_points
  write (*,'(''Average of positive points = '', 1g12.4)') positive_average
  write (*,'(''Average of negative points = '', 1g12.4)') negative_average
 
end program average
")
TestCase("subroutine ccopy ( n, cx, incx, cy, incy )
!
!*******************************************************************************
!
!! CCOPY copies a vector, x, to a vector, y.
!
!
!     jack dongarra, linpack, 3/11/78.
!     modified 12/3/93, array(1) declarations changed to array(*)
!
!  Reference:
!
!    Lawson, Hanson, Kincaid, Krogh,
!    Basic Linear Algebra Subprograms for Fortran Usage,
!    Algorithm 539,
!    ACM Transactions on Mathematical Software,
!    Volume 5, Number 3, September 1979, pages 308-323.
!
  implicit none
!
  complex cx(*)
  complex cy(*)
  integer i
  integer incx
  integer incy
  integer ix
  integer iy
  integer n
!
  if ( n <= 0 ) then
    return
  end if

  if ( incx /= 1 .or. incy /= 1 ) then

    if ( incx >= 0 ) then
      ix = 1
    else
      ix = ( -n + 1 ) * incx + 1
    end if

    if ( incy >= 0 ) then
      iy = 1
    else
      iy = ( -n + 1 ) * incy + 1
    end if

    do i = 1, n
      cy(iy) = cx(ix)
      ix = ix + incx
      iy = iy + incy
    end do

  else

    cy(1:n) = cx(1:n)

  end if

  return
end
")>]
let ``When simple fortran sample then translation works as expected``(testProgram:string) = 
  
    let lexbuf =  Lexing.LexBuffer<byte>.FromString testProgram
    try      
       let Ast =  executable_program tokenize (lexbuf)         
       let Result = Fortran2CSharpTranscompiler.Translate Ast
       System.Console.Out.WriteLine(Result)
       Assert.That(Result.Length, Is.GreaterThan(1000))          
    with e ->        
                //ParseError(e, lexbuf)  
                reraise()               
    ()

[<Test>]
let ``When simple loop statement then simple for loop is made``() = 
    let Ast = {
            Statements = [DoLoop([Equal(Identifier("g"), Expressions([Constant(IntValue(1L)); 
                      Identifier("n")]))],
                      [Expression(Assignment(Identifier("h"), Identifier("g")))])]
               }
    let Result = Fortran2CSharpTranscompiler.Translate Ast
    Assert.That(Result, Is.EqualTo("for(g = 1;g < n;g++){\r\nh = g;\r\n}"))

[<Test>]
let ``When simple loop condition contains label then simple for loop is made``() = 
    let Ast = {
            Statements = [DoLoop([Identifier("label");Equal(Identifier("g"), Expressions([Constant(IntValue(1L)); 
                      Identifier("n")]))],
                      [Expression(Assignment(Identifier("h"), Identifier("g")))])]
               }
    let Result = Fortran2CSharpTranscompiler.Translate Ast
    Assert.That(Result, Is.EqualTo("for(g = 1;g < n;g++){\r\nh = g;\r\n}"))

[<Test>]
let ``When array variable is present then result has array specification``() = 
    let Ast = {
        Statements = [
            Specification([
                VariableDeclaration(Int, Variable(Identifier("k"), [Restriction(IntValue(-1L),IntValue(1L))]),[])
            ])
        ]
    }
    let Result = Fortran2CSharpTranscompiler.Translate Ast
    Assert.That(Result, Is.EqualTo("int[] k;\r\n"))