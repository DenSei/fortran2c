﻿namespace FortranAst

type Value = 
    |FloatValue of float
    |DoubleValue of double
    |StringValue of string
    |IntValue of int64
    |BooleanValue of bool

type Type  = Int | Real |  DoubleType | String | Complex | DoubleComplex | Bool | Char | Void | Undefined of string | ArrayType of Type
type RelOp = EQ | GT | LT | GE | LE | NE
type AlgOp = PlusOp|MinusOp|DivideOp|MultiplyOp|RemainderOp|PowerOp

type Expression = 
    |Assignment of Expression * Expression
    |ObjectInitialization of Type * Expression
    |Identifier of string
    |Variable of Expression * Expression list
    |Continue of string
    |ParameterizedExpression of Expression
    |Label of string
    |Allocate of Expression
    |Deallocate of Expression
    |Goto of Expression list
    |Constant of Value
    |UnaryExpression1 of AlgOp * Expression
    |GreaterOrEqual of Expression * Expression
    |Greater of Expression * Expression
    |LessOrEqual of Expression * Expression
    |Less of Expression * Expression
    |Equal of Expression * Expression
    |NotEqual of Expression * Expression
    |Plus of Expression * Expression
    |Minus of Expression * Expression
    |Divide of Expression * Expression
    |Multiply of Expression * Expression
    |Power of Expression * Expression
    |Remainder of Expression * Expression
    |Not of Expression
    |And of Expression * Expression
    |Or of Expression * Expression
    |Concat of Expression * Expression
    |FunctionCall of string * Expression list
    |ComplexConst of Expression * Expression
    |OtherExpr of string list
    |Restriction of Value * Value
    |Dim of Expression * Expression
    |TypeDefinition of Type
    |Return of Expression
    |Expressions of Expression list
    |OtherVar of string
    |ArithIf of Expression
    |ElseIf of Expression
    |EmptyExpr

type Specification =     
    |VariableDeclaration of Type * Expression * Expression list
    |FunctionDeclaration of string * Type * string list
    |InternalFunctionDeclaration of string * Type * Specification list
    |Equivalence of Expression list
    |Data of Expression list
    |Other of string list

type Statement = 
    |Comment of string
    |Specification of Specification list
    |Execution of string list
    |Expression of Expression
    |If of Expression * Statement list
    |DoLoop of Expression list * Statement list
    |Function of Statement list
    |Block of Statement list  

type FortranProgram = 
    {
        Statements: Statement list
    }