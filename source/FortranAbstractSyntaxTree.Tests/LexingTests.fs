﻿module LexingTests

open NUnit.Framework
open FortranParser
open FortranLexer
open FsUnit

let parse lexbuf =
    let mutable keepParsing = true
    let mutable tokenList = []
    
    while keepParsing = true do
        let parsedToken = tokenize lexbuf
        tokenList <- tokenList @ [parsedToken]    
        if parsedToken = FortranParser.SEOF then
            keepParsing <- false            
    tokenList

let FortranTestProgram = "program average
 
! Read in some numbers and take the average
! As written, if there are no data points, an average of zero is returned
! While this may not be desired behavior, it keeps this example simple
 
  implicit none
  integer :: number_of_points
  real, dimension(:), allocatable :: points
  real :: average_points=0., positive_average=0., negative_average=0.
 
  write (*,*) \"Input number of points to average:\"
  read (*,*) number_of_points
 
  allocate (points(number_of_points))
 
  write (*,*) \"Enter the points to average:\"
  read (*,*) points
 
! Take the average by summing points and dividing by number_of_points
  if (number_of_points > 0) average_points = sum(points)/number_of_points
 
! Now form average over positive and negative points only
  if (count(points > 0.) > 0) positive_average = sum(points, points > 0.) &
        /count(points > 0.)
  if (count(points < 0.) > 0) negative_average = sum(points, points < 0.) &
        /count(points < 0.)
 
  deallocate (points)
 
! Print result to terminal
!  write (*,'(''Average = '', 1g12.4)') average_points
!  write (*,'(''Average of positive points = '', 1g12.4)') positive_average
!  write (*,'(''Average of negative points = '', 1g12.4)') negative_average
 
end program average
"

[<Test>]
let ``WhenSingleProgramIsReadThenTokensAreListed``() =    
    let lexbuf = Lexing.LexBuffer<byte>.FromString FortranTestProgram   
    let y = parse lexbuf 
    y.Length |> should greaterThan 0

[<Test>]
let ``When Simple String Is Given It will be recognized as a string``() =    
    let lexbuf = Lexing.LexBuffer<byte>.FromString "dies nicht \"dafür das\" und das wieder nicht \"dafür wieder das\""   
    let y = parse lexbuf 
    y.Length |> should equal 9
    y.Tail.Tail.Head |> should equal (STRING("\"dafür das\""))

[<Test>]
let ``WhenCommentIsGivenThenLexerRecognizesComment``() =    
    let lexbuf = Lexing.LexBuffer<byte>.FromString "! Dies ist ein langer Kommentar\n"   
    let y = parse lexbuf 
    y.Length |> should equal 3
    y.Head |> should equal (SCOMMENT("! Dies ist ein langer Kommentar"))

[<Test>]
let ``WhenTwoCommentsInARowThenLexerRecognizesTwoComments``() =    
    let lexbuf = Lexing.LexBuffer<byte>.FromString "! Dies ist ein langer Kommentar\n!und noch einer\n"   
    let y = parse lexbuf 
    y.Length |> should equal 5
    y.Head |> should equal (SCOMMENT("! Dies ist ein langer Kommentar"))
    y.Tail.Tail.Head |> should equal (SCOMMENT("!und noch einer"))

[<Test>]
let ``WhenCommentIsFollowedByNormalCodeThenLexerDoesNotMixCommentAndCode``() =    
    let lexbuf = Lexing.LexBuffer<byte>.FromString "! Dies ist ein langer Kommentar\nimplicit none"   
    let y = parse lexbuf 
    y.Length |> should equal 5
    y.Head |> should equal (SCOMMENT("! Dies ist ein langer Kommentar"))
    y.Tail.Tail.Head |> should equal (SIMPLICIT)

[<Test>]
let ``WhenCommentIsGivenWithoutCarriageReturnThenLexerRecognizesComment``() =    
    let lexbuf = Lexing.LexBuffer<byte>.FromString "! Dies ist ein langer Kommentar ohne Carriage Return"   
    let y = parse lexbuf 
    y.Length |> should equal 2
    y.Head |> should equal (SCOMMENT("! Dies ist ein langer Kommentar ohne Carriage Return"))

[<Test>]
let ``WhenStringContainsKeywordInLowerCaseThenTokenIsRecognizedAsKeyword``() = 
    let lexbuf = Lexing.LexBuffer<byte>.FromString "wRitE"
    let y = parse lexbuf
    y.Length |> should equal 2
    y.Head |> should equal SWRITE

[<Test>]
let ``When Unary Minus Then Unary Expression will be build``() = 
    let lexbuf = Lexing.LexBuffer<byte>.FromString "(-n + 1) * r"
    let result = parse lexbuf
    result.Length |> should equal 9
    result.Item(1) |> should equal SMINUS

[<Test>]
let ``When Program Fragment Is Given Then Lexer Can Read In``() = 
    let EmptyProgram = "program average  end program average"
    let lexbuf = Lexing.LexBuffer<byte>.FromString EmptyProgram
    let y = parse lexbuf
    y.Length |> should equal 6
    y.Head |> should equal SPROGRAM
    y.Tail.Tail.Head |> should equal SEND