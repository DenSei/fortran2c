﻿module ParsingTests

open NUnit.Framework
open FortranParser
open FortranLexer
open FsUnit
open FortranAst


let ParseError (e:exn, lexbuf:Lexing.LexBuffer<_>) =
    let pos = lexbuf.EndPos
    let line = pos.Line
    let column = pos.Column
    let message = e.Message
    let lastToken = new System.String(lexbuf.Lexeme)
    printfn "Parse failed at line %d, column %d:\n" line column
    printfn "Last loken: %s" lastToken
    printfn "\n"                

[<Test>]
let ``When Sample Is Given Then AST Contains Nodes and does not throw an error``() =
    let Program = LexingTests.FortranTestProgram
    let lexbuf =  Lexing.LexBuffer<byte>.FromString Program
    try      
       let Nodes =  executable_program tokenize (lexbuf)      
       Nodes.Statements.Length |>  should equal 1       
    with e ->        
                ParseError(e, lexbuf)  
                reraise()               
    ()

[<Test>]
let ``When Empty Program Is Given Then Ast Contains Empty Program Signature``() = 
    let EmptyProgram = "program average 
    end program average\n"
    let lexbuf =  Lexing.LexBuffer<byte>.FromString EmptyProgram
    try      
       let Nodes =  executable_program tokenize (lexbuf)
      
       printf "%s" (Nodes.ToString())
    with e ->        
                ParseError(e, lexbuf)  
                reraise()               
    ()
 
//Test funktioniert aktuell nur, wenn in der Grammatik program_fragment aktiviert ist.
[<Test>]
let ``When assignment then parser creates assignment expression``() =  
    let TestAssignment = @"positive_average = sum(points, points > 0.) /count(points > 0.)"
    let lexbuf =  Lexing.LexBuffer<byte>.FromString TestAssignment  
    //let Tokens = LexingTests.parse lexbuf       
    let Ast =  executable_program tokenize (lexbuf)
    Ast.Statements.Length |> should equal 1

[<Test>]
let ``When complex write function call with several string concatenations then parser is able to resolve it``() =
    let TestWriteFunctionCall = @"write (*,'(''Average of positive points = '', 1g12.4)') positive_average
                                write (*,'(''Average of positive points = '', 1g12.4)') positive_average
                                "
    let lexbuf = Lexing.LexBuffer<byte>.FromString TestWriteFunctionCall
    //let Tokens = LexingTests.parse lexbuf 
    let Ast = executable_program tokenize lexbuf
    Ast.Statements.Length |> should equal 2
    Assert.That(Ast.Statements.Head, Is.EqualTo(Expression(FunctionCall("write",
                                    [Constant(StringValue("*"));
                                    Constant(StringValue("'(''Average of positive points = '', 1g12.4)'"));
                                    Identifier("positive_average")]))))

[<Test>]
let ``When unary expression is existing then unary expression is part of statement``() = 
    let UnaryExpression = "k = (-n + 1) * 3
"
    let lexbuf = Lexing.LexBuffer<byte>.FromString UnaryExpression
    let Ast = executable_program tokenize lexbuf 
    Assert.That(Ast.Statements.Head, Is.EqualTo(Expression(Assignment(Identifier("k"),Multiply(
            Plus(UnaryExpression1(AlgOp.MinusOp, Identifier("n")),Constant(IntValue(1L))), 
            Constant(IntValue(3L)))))))    

[<Test>]
let ``When array copy expression then parser generates two function calls``() = 
    let Eingabe ="cy(1:n) = cx(1:n)\r\n"
    let lexbuf = Lexing.LexBuffer<byte>.FromString Eingabe
    let Ast = executable_program tokenize lexbuf 
    Assert.That(Ast.Statements.Head, 
    Is.EqualTo(Expression(Assignment(FunctionCall("cy", [Dim(Constant(IntValue(1L)), Identifier("n"))]), 
    FunctionCall("cx", [Dim(Constant(IntValue(1L)), Identifier("n"))])))))