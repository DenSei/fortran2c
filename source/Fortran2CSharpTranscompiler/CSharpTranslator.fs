﻿module Fortran2CSharpTranscompiler

open FortranAst
open System
open System.Text


let MapAlgOp algop = 
    match algop with
    |PlusOp -> "+"
    |MinusOp -> "-"
    |DivideOp -> "/"
    |MultiplyOp -> "*"
    |RemainderOp -> "%"
    |PowerOp -> "Math.Pow"

let BuildSelection(identifier:string, expression:Expression, translateExpressionDelegate:Expression->string) = 
    String.Format(".Where(_{0} => {1})",identifier, translateExpressionDelegate(expression).Replace(identifier, "_"+identifier))

let BuildArrayAggregateFunction(functionName:string, arguments: Expression list, translateExpressionDelegate:Expression -> string) = 
    let Identifier = match arguments.Head with | Identifier(name) -> name
                                               | Greater(Identifier(name), _) -> name
                                               | GreaterOrEqual(Identifier(name), _) -> name
                                               | Less(Identifier(name), _) -> name
                                               | LessOrEqual(Identifier(name), _) -> name
                                               | Equal(Identifier(name), _) -> name
                                               | NotEqual(Identifier(name), _) -> name
    let Builder = new StringBuilder()
    for i = 1 to arguments.Length-1 do
        Builder.Append(match arguments.Item(i) with 
        |Constant(IntValue(value)) -> ""
        |_ -> BuildSelection(Identifier, arguments.Item(i), translateExpressionDelegate)
        )|> ignore
    Builder.Insert(0, Identifier) |> ignore
    Builder.AppendFormat(".{0}()",functionName) |> ignore
    Builder.ToString()

let rec GetIdentifier exprs = 
    match exprs with 
    |Identifier(name) :: tail -> name
    |_ :: tail -> GetIdentifier tail
    |[] -> String.Empty
    
let BuildIntrinsicFunction(name:string, arguments:Expression list, translateExpressionDelegate:Expression -> string) = 
    match name with 
        |"sum" -> BuildArrayAggregateFunction("Sum",arguments, translateExpressionDelegate)
        |"count" -> BuildArrayAggregateFunction("Count",arguments, translateExpressionDelegate)
        |"read" -> String.Format("{0} = Console.In.ReadLine()\r\n", GetIdentifier arguments)
        |"write" -> String.Format("Console.Out.WriteLine({0})\r\n", GetIdentifier arguments)
        |_ -> String.Format("{0}({1})", name, String.Join(",",List.map (fun arg -> translateExpressionDelegate arg) arguments)) 

let RebuildFunctionStatement (functionStatements:Statement list) = 
    let FunctionDeclaration = match functionStatements.Head with
                                | Specification specifications -> 
                                    match specifications.Head with 
                                        |FunctionDeclaration(name,returnType, arguments) -> (name, returnType, arguments)
    let First (a,_,_) = a
    let Second (_,b,_) = b
    let Third (_,_,c) = c
    let ArgumentsOfFunction = Third FunctionDeclaration
    let Arguments = List.choose(fun z -> match z with 
                        | Specification specifications -> Some(specifications)
                        | _ -> None) functionStatements |> List.collect(fun x -> x) |>
                        List.choose(fun y -> match y with 
                            |VariableDeclaration(_, expression, _) -> 
                                    match expression with 
                                    |Identifier name -> if List.exists(fun argument -> argument = name) ArgumentsOfFunction then Some(y) else None
                                    |Variable(Identifier name, _) -> if List.exists(fun argument -> argument = name) ArgumentsOfFunction then Some(y) else None
                                    |_ -> None
                            | _ -> None)
    let InternalFunctionDeclaration = Specification([InternalFunctionDeclaration(First FunctionDeclaration, Second FunctionDeclaration, Arguments)])
    let Contains statement arguments = 
        match statement with
        | Specification specifications -> List.exists(fun spec -> match spec with 
                                          |VariableDeclaration(_, expression, _) -> match expression with 
                                                                                |Identifier name -> if List.exists(fun argument -> argument = name) arguments then true else false
                                                                                |Variable(Identifier name, _) ->if List.exists(fun argument -> argument = name) arguments then true else false
                                                                                |_ -> false
                                          | _ -> false) specifications
        | _ -> false
    InternalFunctionDeclaration :: List.filter(fun item -> not(Contains item ArgumentsOfFunction)) functionStatements.Tail   

let rec TranslateType asttype = 
    match asttype with
        |String -> "string"
        |Real|DoubleType -> "double"
        |Complex|DoubleComplex -> "complex"
        |Bool -> "bool"
        |Char -> "char"
        |Void -> "void"
        |Int -> "int"
        |ArrayType valuetype -> TranslateType valuetype
        | _ -> "/*TODO: unknown variable type*/"

let TranslateValue value = 
    match value with 
        |FloatValue x -> x.ToString(System.Globalization.CultureInfo.InvariantCulture)
        |IntValue x -> x.ToString(System.Globalization.CultureInfo.InvariantCulture)
        |DoubleValue x -> x.ToString(System.Globalization.CultureInfo.InvariantCulture)
        |StringValue x -> "\""+x+"\""
        |_ -> "/*TODO: Cannot translate value type.*/"


let rec TranslateExpression expression = 
    match expression with
        |Assignment(FunctionCall(name1, [Dim(fdim1, fdim2)]), FunctionCall(name2, [Dim(fdim3, fdim4)])) -> String.Format("Array.Copy({0},{1},{2})",name2,name1,TranslateExpression(Minus(Plus(fdim4, Constant(IntValue(1L))), fdim3)))
        |Assignment (name, value) -> String.Format("{0} = {1}", TranslateExpression name, TranslateExpression value)       
        |Identifier name -> name
        |Constant value -> TranslateValue value
        |Equal(expr1, expr2) -> String.Format("{0} == {1}", TranslateExpression expr1, TranslateExpression expr2)
        |NotEqual(expr1, expr2) -> String.Format("{0} != {1}", TranslateExpression expr1, TranslateExpression expr2)
        |Greater(expr1, expr2) -> String.Format("{0} > {1}", TranslateExpression expr1, TranslateExpression expr2)
        |GreaterOrEqual(expr1, expr2) -> String.Format("{0} >= {1}", TranslateExpression expr1, TranslateExpression expr2)
        |Less(expr1, expr2) -> String.Format("{0} < {1}", TranslateExpression expr1, TranslateExpression expr2)
        |LessOrEqual(expr1, expr2) -> String.Format("{0} <= {1}", TranslateExpression expr1, TranslateExpression expr2)
        |Or(expr1, expr2) -> String.Format("{0} || {1}", TranslateExpression expr1, TranslateExpression expr2)
        |And(expr1, expr2) -> String.Format("{0} && {1}", TranslateExpression expr1, TranslateExpression expr2)
        |Not expr -> String.Format("!{0}", TranslateExpression expr)
        |FunctionCall(name, exprs) -> BuildIntrinsicFunction(name, exprs, TranslateExpression)
        |Divide(expr1, expr2) -> String.Format("{0} / {1}",TranslateExpression expr1, TranslateExpression expr2)
        |Multiply(expr1, expr2) -> String.Format("{0} * {1}",TranslateExpression expr1, TranslateExpression expr2)
        |Plus(expr1, expr2) -> String.Format("{0} + {1}",TranslateExpression expr1, TranslateExpression expr2)
        |Minus(expr1, expr2) -> String.Format("{0} - {1}",TranslateExpression expr1, TranslateExpression expr2)
        |Allocate expr -> match expr with | FunctionCall(name, exprs) -> TranslateExpression(Assignment(Identifier(name), ObjectInitialization(ArrayType(DoubleType), exprs.Head)))
        |ObjectInitialization(valueType, expr) -> match valueType with 
                                                | ArrayType arrayType -> String.Format("new {0}[{1}]", TranslateType arrayType, TranslateExpression expr)
                                                | _ -> String.Format("new {0}({1})", TranslateType valueType, TranslateExpression expr)                                                
        |ParameterizedExpression expr -> String.Format("({0})", TranslateExpression expr)
        |Return expr -> String.Format("return {0};\r\n", TranslateExpression expr)
        |UnaryExpression1(op, expr) -> String.Format("{0}{1}", MapAlgOp op, TranslateExpression expr)        
        |Variable(Identifier(name), _) -> name
        |EmptyExpr -> String.Empty
        |_ -> "/*TODO: cannot recognize expression.*/"

let TranslateVariableAttribute attribute = 
    match attribute with 
    |Restriction (lowerBound, upperBound) -> "[]"
    | _ -> String.Empty

let rec TranslateSpecification specification = 
    match specification with
        |InternalFunctionDeclaration (name, returnType, arguments) -> String.Format("public {0} {1}({2}) {{",TranslateType(returnType), name, String.Join(",", List.map TranslateSpecification arguments).Replace(";", String.Empty)) 
        |VariableDeclaration (variableType, expression, attributes) -> 
            let ArrayDeclaration = match expression with | Variable(_, [Restriction(_,_)]) -> "[]" | _ -> String.Empty
            String.Format("{0}{1} {2}{3};", TranslateType(variableType), ArrayDeclaration, TranslateExpression(expression), String.Join(String.Empty, List.map TranslateVariableAttribute attributes))
        | _ -> "//TODO: cannot translate specification."

let rec TranslateStatement statement = 
    match statement with 
        |Comment x -> x.Insert(0, "//")+Environment.NewLine
        |Function statements -> let Builder = new StringBuilder()
                                let ReorganizedStatements = RebuildFunctionStatement statements                                                                                                
                                List.map TranslateStatement  ReorganizedStatements |> List.iter (fun e-> Builder.Append(e) |> ignore)
                                Builder.AppendLine("}").ToString()
        |Specification specifications -> let Builder = new StringBuilder() 
                                         List.map TranslateSpecification specifications |> List.iter (fun e-> Builder.AppendLine(e) |> ignore)
                                         Builder.ToString()
        |If(expr, statements) -> let Builder = new StringBuilder()
                                 let IfClause = match expr with 
                                                |EmptyExpr -> "else {"
                                                |ElseIf x -> String.Format("else if({0}) {{", TranslateExpression x)
                                                |_ -> String.Format("if({0}) {{", TranslateExpression expr)                                         
                                 Builder.AppendLine(IfClause) |> ignore
                                 List.map TranslateStatement statements |> List.iter (fun e -> Builder.AppendLine(e) |> ignore)
                                 Builder.AppendLine("}") |> ignore
                                 Builder.ToString()
        |Block statements -> let Builder = new StringBuilder()
                             List.map TranslateStatement statements |> List.iter (fun e-> Builder.Append(e) |> ignore)
                             Builder.ToString()
        |DoLoop(condition, statements) -> 
                            let Builder = new StringBuilder()
                            List.map TranslateStatement statements |> List.iter (fun e-> Builder.AppendLine(e) |> ignore)                            
                            let Identifier = match condition.Item(condition.Length-1) with | Equal(left,_) -> left| _ -> raise(new ArgumentException("Expected assignment in loop condition expression."))
                            let AssignmentAndBoundary = match condition.Item(condition.Length-1) with | Equal(_,Expressions(right)) -> (Assignment(Identifier, right.Head),Less(Identifier, right.Item(right.Length-1))) | _ -> raise(new ArgumentException("Cannot build assignment expression in do- loop."))                            
                            //Assignment bringt schon das ; mit
                            String.Format("for({0};{1};{2}){{\r\n{3}}}",
                                        TranslateExpression(fst(AssignmentAndBoundary)),
                                        TranslateExpression(snd(AssignmentAndBoundary)),
                                        String.Format("{0}++",TranslateExpression Identifier), 
                                        Builder.ToString())
        |Expression expr -> (TranslateExpression expr)+";"
        | _ -> "//TODO: does not recognized statement. Take a look into source document to verify."                       

let Translate Ast = 
    let Builder = new StringBuilder()
    //Builder.AppendLine("using System;") |> ignore
    Ast.Statements |> List.map TranslateStatement |> List.iter (fun e-> Builder.Append(e) |> ignore)
    //Builder.Append(System.Text.RegularExpressions.Regex.Replace(e,"[^{}][\r]*\n", ";\r\n")) |> ignore)     
    Builder.ToString()    
